
package osacbm.module;
import ice.*;

public class Server {
public static void main(String[] args)
{
    
int status = 0;
Ice.Communicator ic = null;

// contains all server code in try block
try {
System.out.println("Server is ready");
ic = Ice.Util.initialize(args);         
Ice.ObjectAdapter adapter =
ic.createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -p 10000");
// Create ice object
Ice.Object object = new PrinterI();

adapter.add(object, ic.stringToIdentity("SimplePrinter"));
// All objects are created, allow client requests now.
adapter.activate();
// Wait until we are done.
ic.waitForShutdown();
} 



catch (Ice.LocalException e) {
e.printStackTrace();
status = 1;
} 
catch (Exception e) {
System.err.println(e.getMessage());
status = 1;
}
if (ic != null) {
// Clean up
//
try {
ic.destroy();
} 
catch (Exception e) {
System.err.println(e.getMessage());
status = 1;
}
}
System.exit(status);
}
}


