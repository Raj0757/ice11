// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class AlertType extends Ice.ObjectImpl
{
    public AlertType()
    {
    }

    public AlertType(Site alertTypeSite, LongValue alertTypeId, LongValue alertTypeCode, MIMKey3 alertSeverity, String alertName)
    {
        this.alertTypeSite = alertTypeSite;
        this.alertTypeId = alertTypeId;
        this.alertTypeCode = alertTypeCode;
        this.alertSeverity = alertSeverity;
        this.alertName = alertName;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new AlertType();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::AlertType"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(alertTypeSite);
        __os.writeObject(alertTypeId);
        __os.writeObject(alertTypeCode);
        __os.writeObject(alertSeverity);
        __os.writeString(alertName);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::Site";
                if(v == null || v instanceof Site)
                {
                    alertTypeSite = (Site)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::LongValue";
                if(v == null || v instanceof LongValue)
                {
                    alertTypeId = (LongValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::LongValue";
                if(v == null || v instanceof LongValue)
                {
                    alertTypeCode = (LongValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 3:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    alertSeverity = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.readObject(new Patcher(3));
        alertName = __is.readString();
        __is.endReadSlice();
    }

    protected Site alertTypeSite;

    public Site
    getAlertTypeSite()
    {
        return alertTypeSite;
    }

    public void
    setAlertTypeSite(Site _alertTypeSite)
    {
        alertTypeSite = _alertTypeSite;
    }

    protected LongValue alertTypeId;

    public LongValue
    getAlertTypeId()
    {
        return alertTypeId;
    }

    public void
    setAlertTypeId(LongValue _alertTypeId)
    {
        alertTypeId = _alertTypeId;
    }

    protected LongValue alertTypeCode;

    public LongValue
    getAlertTypeCode()
    {
        return alertTypeCode;
    }

    public void
    setAlertTypeCode(LongValue _alertTypeCode)
    {
        alertTypeCode = _alertTypeCode;
    }

    protected MIMKey3 alertSeverity;

    public MIMKey3
    getAlertSeverity()
    {
        return alertSeverity;
    }

    public void
    setAlertSeverity(MIMKey3 _alertSeverity)
    {
        alertSeverity = _alertSeverity;
    }

    protected String alertName;

    public String
    getAlertName()
    {
        return alertName;
    }

    public void
    setAlertName(String _alertName)
    {
        alertName = _alertName;
    }

    public static final long serialVersionUID = -230636151L;
}
