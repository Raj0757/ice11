// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class ExplanationSrcsStr extends Explanation
{
    public ExplanationSrcsStr()
    {
        super();
    }

    public ExplanationSrcsStr(BooleanValue used, java.util.List<EntryPointStringfield> strEntryPoIntValues)
    {
        super(used);
        this.strEntryPoIntValues = strEntryPoIntValues;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new ExplanationSrcsStr();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::Explanation",
        "::osacbm::schema::ExplanationSrcsStr"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[2];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[2];
    }

    public static String ice_staticId()
    {
        return __ids[2];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, false);
        EntryPointStringfieldSeqHelper.write(__os, strEntryPoIntValues);
        __os.endWriteSlice();
        super.__writeImpl(__os);
    }

    private class Patcher implements IceInternal.Patcher
    {
        public void
        patch(Ice.Object v)
        {
            if(v == null || v instanceof BooleanValue)
            {
                used = (BooleanValue)v;
            }
            else
            {
                IceInternal.Ex.throwUOE(type(), v);
            }
        }

        public String
        type()
        {
            return "::osacbm::schema::BooleanValue";
        }
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        strEntryPoIntValues = EntryPointStringfieldSeqHelper.read(__is);
        __is.endReadSlice();
        super.__readImpl(__is);
    }

    protected java.util.List<EntryPointStringfield> strEntryPoIntValues;

    public java.util.List<EntryPointStringfield>
    getStrEntryPoIntValues()
    {
        return strEntryPoIntValues;
    }

    public void
    setStrEntryPoIntValues(java.util.List<EntryPointStringfield> _strEntryPoIntValues)
    {
        strEntryPoIntValues = _strEntryPoIntValues;
    }

    public static final long serialVersionUID = -2100085113L;
}
