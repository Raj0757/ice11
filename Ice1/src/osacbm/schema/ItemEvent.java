// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class ItemEvent extends Ice.ObjectImpl
{
    public ItemEvent()
    {
    }

    public ItemEvent(ItemId itemIdValue, MIMKey3 eventType, OsacbmTime eventStart, OsacbmTime eventStop, String name, String userTag, java.util.List<ItemEventNumInt> itemEventNumIntVal, java.util.List<ItemEventChar> itemEventCharValue, java.util.List<ItemEventNumReal> itemEventNumRealVal, java.util.List<ItemEventBLOB> itemEventBLOBVal)
    {
        this.itemIdValue = itemIdValue;
        this.eventType = eventType;
        this.eventStart = eventStart;
        this.eventStop = eventStop;
        this.name = name;
        this.userTag = userTag;
        this.itemEventNumIntVal = itemEventNumIntVal;
        this.itemEventCharValue = itemEventCharValue;
        this.itemEventNumRealVal = itemEventNumRealVal;
        this.itemEventBLOBVal = itemEventBLOBVal;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new ItemEvent();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::ItemEvent"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(itemIdValue);
        __os.writeObject(eventType);
        __os.writeObject(eventStart);
        __os.writeObject(eventStop);
        __os.writeString(name);
        __os.writeString(userTag);
        ItemEventNumIntSeqHelper.write(__os, itemEventNumIntVal);
        ItemEventCharSeqHelper.write(__os, itemEventCharValue);
        ItemEventNumRealSeqHelper.write(__os, itemEventNumRealVal);
        ItemEventBLOBSeqHelper.write(__os, itemEventBLOBVal);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::ItemId";
                if(v == null || v instanceof ItemId)
                {
                    itemIdValue = (ItemId)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    eventType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::OsacbmTime";
                if(v == null || v instanceof OsacbmTime)
                {
                    eventStart = (OsacbmTime)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 3:
                __typeId = "::osacbm::schema::OsacbmTime";
                if(v == null || v instanceof OsacbmTime)
                {
                    eventStop = (OsacbmTime)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.readObject(new Patcher(3));
        name = __is.readString();
        userTag = __is.readString();
        itemEventNumIntVal = ItemEventNumIntSeqHelper.read(__is);
        itemEventCharValue = ItemEventCharSeqHelper.read(__is);
        itemEventNumRealVal = ItemEventNumRealSeqHelper.read(__is);
        itemEventBLOBVal = ItemEventBLOBSeqHelper.read(__is);
        __is.endReadSlice();
    }

    protected ItemId itemIdValue;

    public ItemId
    getItemIdValue()
    {
        return itemIdValue;
    }

    public void
    setItemIdValue(ItemId _itemIdValue)
    {
        itemIdValue = _itemIdValue;
    }

    protected MIMKey3 eventType;

    public MIMKey3
    getEventType()
    {
        return eventType;
    }

    public void
    setEventType(MIMKey3 _eventType)
    {
        eventType = _eventType;
    }

    protected OsacbmTime eventStart;

    public OsacbmTime
    getEventStart()
    {
        return eventStart;
    }

    public void
    setEventStart(OsacbmTime _eventStart)
    {
        eventStart = _eventStart;
    }

    protected OsacbmTime eventStop;

    public OsacbmTime
    getEventStop()
    {
        return eventStop;
    }

    public void
    setEventStop(OsacbmTime _eventStop)
    {
        eventStop = _eventStop;
    }

    protected String name;

    public String
    getName()
    {
        return name;
    }

    public void
    setName(String _name)
    {
        name = _name;
    }

    protected String userTag;

    public String
    getUserTag()
    {
        return userTag;
    }

    public void
    setUserTag(String _userTag)
    {
        userTag = _userTag;
    }

    protected java.util.List<ItemEventNumInt> itemEventNumIntVal;

    public java.util.List<ItemEventNumInt>
    getItemEventNumIntVal()
    {
        return itemEventNumIntVal;
    }

    public void
    setItemEventNumIntVal(java.util.List<ItemEventNumInt> _itemEventNumIntVal)
    {
        itemEventNumIntVal = _itemEventNumIntVal;
    }

    protected java.util.List<ItemEventChar> itemEventCharValue;

    public java.util.List<ItemEventChar>
    getItemEventCharValue()
    {
        return itemEventCharValue;
    }

    public void
    setItemEventCharValue(java.util.List<ItemEventChar> _itemEventCharValue)
    {
        itemEventCharValue = _itemEventCharValue;
    }

    protected java.util.List<ItemEventNumReal> itemEventNumRealVal;

    public java.util.List<ItemEventNumReal>
    getItemEventNumRealVal()
    {
        return itemEventNumRealVal;
    }

    public void
    setItemEventNumRealVal(java.util.List<ItemEventNumReal> _itemEventNumRealVal)
    {
        itemEventNumRealVal = _itemEventNumRealVal;
    }

    protected java.util.List<ItemEventBLOB> itemEventBLOBVal;

    public java.util.List<ItemEventBLOB>
    getItemEventBLOBVal()
    {
        return itemEventBLOBVal;
    }

    public void
    setItemEventBLOBVal(java.util.List<ItemEventBLOB> _itemEventBLOBVal)
    {
        itemEventBLOBVal = _itemEventBLOBVal;
    }

    public static final long serialVersionUID = -1483415343L;
}
