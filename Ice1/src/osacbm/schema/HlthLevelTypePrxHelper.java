// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class HlthLevelTypePrxHelper extends Ice.ObjectPrxHelperBase implements HlthLevelTypePrx
{
    public static HlthLevelTypePrx checkedCast(Ice.ObjectPrx __obj)
    {
        HlthLevelTypePrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof HlthLevelTypePrx)
            {
                __d = (HlthLevelTypePrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    HlthLevelTypePrxHelper __h = new HlthLevelTypePrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static HlthLevelTypePrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        HlthLevelTypePrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof HlthLevelTypePrx)
            {
                __d = (HlthLevelTypePrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    HlthLevelTypePrxHelper __h = new HlthLevelTypePrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static HlthLevelTypePrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        HlthLevelTypePrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    HlthLevelTypePrxHelper __h = new HlthLevelTypePrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static HlthLevelTypePrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        HlthLevelTypePrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    HlthLevelTypePrxHelper __h = new HlthLevelTypePrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static HlthLevelTypePrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        HlthLevelTypePrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof HlthLevelTypePrx)
            {
                __d = (HlthLevelTypePrx)__obj;
            }
            else
            {
                HlthLevelTypePrxHelper __h = new HlthLevelTypePrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static HlthLevelTypePrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        HlthLevelTypePrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            HlthLevelTypePrxHelper __h = new HlthLevelTypePrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::HlthLevelType"
    };

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _HlthLevelTypeDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _HlthLevelTypeDelD();
    }

    public static void __write(IceInternal.BasicStream __os, HlthLevelTypePrx v)
    {
        __os.writeProxy(v);
    }

    public static HlthLevelTypePrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            HlthLevelTypePrxHelper result = new HlthLevelTypePrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
