// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class ItemRecommendation extends Ice.ObjectImpl
{
    public ItemRecommendation()
    {
    }

    public ItemRecommendation(ItemId itemIdValue, OsacbmTime utcRecommendation, MIMAgent by, MIMKey3 priorityTypeCode, String userTag, String name, ItemRequestForWork itemRequestForWorkVal, java.util.List<ItemRecommendationRemark> itemRecommendationRemarks)
    {
        this.itemIdValue = itemIdValue;
        this.utcRecommendation = utcRecommendation;
        this.by = by;
        this.priorityTypeCode = priorityTypeCode;
        this.userTag = userTag;
        this.name = name;
        this.itemRequestForWorkVal = itemRequestForWorkVal;
        this.itemRecommendationRemarks = itemRecommendationRemarks;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new ItemRecommendation();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::ItemRecommendation"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(itemIdValue);
        __os.writeObject(utcRecommendation);
        __os.writeObject(by);
        __os.writeObject(priorityTypeCode);
        __os.writeString(userTag);
        __os.writeString(name);
        __os.writeObject(itemRequestForWorkVal);
        ItemRecommendationRemarkSeqHelper.write(__os, itemRecommendationRemarks);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::ItemId";
                if(v == null || v instanceof ItemId)
                {
                    itemIdValue = (ItemId)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::OsacbmTime";
                if(v == null || v instanceof OsacbmTime)
                {
                    utcRecommendation = (OsacbmTime)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::MIMAgent";
                if(v == null || v instanceof MIMAgent)
                {
                    by = (MIMAgent)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 3:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    priorityTypeCode = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 4:
                __typeId = "::osacbm::schema::ItemRequestForWork";
                if(v == null || v instanceof ItemRequestForWork)
                {
                    itemRequestForWorkVal = (ItemRequestForWork)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.readObject(new Patcher(3));
        userTag = __is.readString();
        name = __is.readString();
        __is.readObject(new Patcher(4));
        itemRecommendationRemarks = ItemRecommendationRemarkSeqHelper.read(__is);
        __is.endReadSlice();
    }

    protected ItemId itemIdValue;

    public ItemId
    getItemIdValue()
    {
        return itemIdValue;
    }

    public void
    setItemIdValue(ItemId _itemIdValue)
    {
        itemIdValue = _itemIdValue;
    }

    protected OsacbmTime utcRecommendation;

    public OsacbmTime
    getUtcRecommendation()
    {
        return utcRecommendation;
    }

    public void
    setUtcRecommendation(OsacbmTime _utcRecommendation)
    {
        utcRecommendation = _utcRecommendation;
    }

    protected MIMAgent by;

    public MIMAgent
    getBy()
    {
        return by;
    }

    public void
    setBy(MIMAgent _by)
    {
        by = _by;
    }

    protected MIMKey3 priorityTypeCode;

    public MIMKey3
    getPriorityTypeCode()
    {
        return priorityTypeCode;
    }

    public void
    setPriorityTypeCode(MIMKey3 _priorityTypeCode)
    {
        priorityTypeCode = _priorityTypeCode;
    }

    protected String userTag;

    public String
    getUserTag()
    {
        return userTag;
    }

    public void
    setUserTag(String _userTag)
    {
        userTag = _userTag;
    }

    protected String name;

    public String
    getName()
    {
        return name;
    }

    public void
    setName(String _name)
    {
        name = _name;
    }

    protected ItemRequestForWork itemRequestForWorkVal;

    public ItemRequestForWork
    getItemRequestForWorkVal()
    {
        return itemRequestForWorkVal;
    }

    public void
    setItemRequestForWorkVal(ItemRequestForWork _itemRequestForWorkVal)
    {
        itemRequestForWorkVal = _itemRequestForWorkVal;
    }

    protected java.util.List<ItemRecommendationRemark> itemRecommendationRemarks;

    public java.util.List<ItemRecommendationRemark>
    getItemRecommendationRemarks()
    {
        return itemRecommendationRemarks;
    }

    public void
    setItemRecommendationRemarks(java.util.List<ItemRecommendationRemark> _itemRecommendationRemarks)
    {
        itemRecommendationRemarks = _itemRecommendationRemarks;
    }

    public static final long serialVersionUID = -1049387643L;
}
