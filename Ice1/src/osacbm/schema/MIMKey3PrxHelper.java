// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class MIMKey3PrxHelper extends Ice.ObjectPrxHelperBase implements MIMKey3Prx
{
    public static MIMKey3Prx checkedCast(Ice.ObjectPrx __obj)
    {
        MIMKey3Prx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof MIMKey3Prx)
            {
                __d = (MIMKey3Prx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    MIMKey3PrxHelper __h = new MIMKey3PrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static MIMKey3Prx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        MIMKey3Prx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof MIMKey3Prx)
            {
                __d = (MIMKey3Prx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    MIMKey3PrxHelper __h = new MIMKey3PrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static MIMKey3Prx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        MIMKey3Prx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    MIMKey3PrxHelper __h = new MIMKey3PrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static MIMKey3Prx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        MIMKey3Prx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    MIMKey3PrxHelper __h = new MIMKey3PrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static MIMKey3Prx uncheckedCast(Ice.ObjectPrx __obj)
    {
        MIMKey3Prx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof MIMKey3Prx)
            {
                __d = (MIMKey3Prx)__obj;
            }
            else
            {
                MIMKey3PrxHelper __h = new MIMKey3PrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static MIMKey3Prx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        MIMKey3Prx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            MIMKey3PrxHelper __h = new MIMKey3PrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::MIMKey3"
    };

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _MIMKey3DelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _MIMKey3DelD();
    }

    public static void __write(IceInternal.BasicStream __os, MIMKey3Prx v)
    {
        __os.writeProxy(v);
    }

    public static MIMKey3Prx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            MIMKey3PrxHelper result = new MIMKey3PrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
