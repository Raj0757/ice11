// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class Metric extends Ice.ObjectImpl
{
    public Metric()
    {
    }

    public Metric(MIMKey3 metricId, FloatValue value, EngUnit metricEU)
    {
        this.metricId = metricId;
        this.value = value;
        this.metricEU = metricEU;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new Metric();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::Metric"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(metricId);
        __os.writeObject(value);
        __os.writeObject(metricEU);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    metricId = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::FloatValue";
                if(v == null || v instanceof FloatValue)
                {
                    value = (FloatValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::EngUnit";
                if(v == null || v instanceof EngUnit)
                {
                    metricEU = (EngUnit)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.endReadSlice();
    }

    protected MIMKey3 metricId;

    public MIMKey3
    getMetricId()
    {
        return metricId;
    }

    public void
    setMetricId(MIMKey3 _metricId)
    {
        metricId = _metricId;
    }

    protected FloatValue value;

    public FloatValue
    getValue()
    {
        return value;
    }

    public void
    setValue(FloatValue _value)
    {
        value = _value;
    }

    protected EngUnit metricEU;

    public EngUnit
    getMetricEU()
    {
        return metricEU;
    }

    public void
    setMetricEU(EngUnit _metricEU)
    {
        metricEU = _metricEU;
    }

    public static final long serialVersionUID = -46924316L;
}
