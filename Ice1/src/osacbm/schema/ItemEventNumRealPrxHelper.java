// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class ItemEventNumRealPrxHelper extends Ice.ObjectPrxHelperBase implements ItemEventNumRealPrx
{
    public static ItemEventNumRealPrx checkedCast(Ice.ObjectPrx __obj)
    {
        ItemEventNumRealPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof ItemEventNumRealPrx)
            {
                __d = (ItemEventNumRealPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    ItemEventNumRealPrxHelper __h = new ItemEventNumRealPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static ItemEventNumRealPrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        ItemEventNumRealPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof ItemEventNumRealPrx)
            {
                __d = (ItemEventNumRealPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    ItemEventNumRealPrxHelper __h = new ItemEventNumRealPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static ItemEventNumRealPrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        ItemEventNumRealPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    ItemEventNumRealPrxHelper __h = new ItemEventNumRealPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static ItemEventNumRealPrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        ItemEventNumRealPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    ItemEventNumRealPrxHelper __h = new ItemEventNumRealPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static ItemEventNumRealPrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        ItemEventNumRealPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof ItemEventNumRealPrx)
            {
                __d = (ItemEventNumRealPrx)__obj;
            }
            else
            {
                ItemEventNumRealPrxHelper __h = new ItemEventNumRealPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static ItemEventNumRealPrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        ItemEventNumRealPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            ItemEventNumRealPrxHelper __h = new ItemEventNumRealPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::ItemEventNumReal"
    };

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _ItemEventNumRealDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _ItemEventNumRealDelD();
    }

    public static void __write(IceInternal.BasicStream __os, ItemEventNumRealPrx v)
    {
        __os.writeProxy(v);
    }

    public static ItemEventNumRealPrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            ItemEventNumRealPrxHelper result = new ItemEventNumRealPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
