#ifndef OSACBM_SCHEMA
#define OSACBM_SCHEMA

module osacbm
{
	module schema {
		
			class ItemRecommendation;
			class DoubleValue;
			class LongValue;
			class BigIntValue;
			class BooleanValue;
			class IntValue;
			class ByteValue;
			class FloatValue;
			class ShortValue;
			class Algorithm;
			class AlgorithmInputData;
			class AlgorithmOutput;
			class AlgorithmInputInt;
			class AlgorithmInputReal;
			class AlgorithmInputChar;
			class AlgorithmModel;
			class LogicalConnector;
			class Parameter;
			class DataSource;
			class Transducer;
			class StringValue;
			class ModuleRef;
			class OutPortSet;
			class ModuleDescriptor;
			class NumAlert;
			class AlertRegion;
			class Data;
			class DataEvent;
			class Value;
			class DataEventSet;
			class DataRef;
			class ExplanationDataRef;
			class ExplanationData;
			class Item;
			class ItemHealth;
			class ItemEventNumInt;
			class ItemEventChar;
			class ItemEventNumReal;
			class ItemEventBLOB;
			class ItemRecommendationRemark;
			class Metric;
			class PortRef;
			class Role;
			class SelectionFilter;
			class MonitorId;
			class MonitorIdGroup;
			class ItemPrognosis;
			class Port;
			class MIMKey3;
			class Function;
			class SDEnumSetDataItem;
			class ItemEvent;
			class ItemEventConfig;
			class MIMAgent;
			class EntryPointStringfield;

			["java:type:java.util.ArrayList<ItemRecommendation>"]
			sequence<ItemRecommendation> ItemRecommendationSeq;
	  
			["java:type:java.util.ArrayList<BooleanValue>"]
			sequence<BooleanValue> BoolSeq;
	
			["java:type:java.util.ArrayList<ByteValue>"]
			sequence<ByteValue> ByteSeq;
	
			["java:type:java.util.ArrayList<IntValue>"]
			sequence<IntValue> IntSeq;
	
			["java:type:java.util.ArrayList<ShortValue>"]
    		sequence<ShortValue> ShortSeq;
    
    		["java:type:java.util.ArrayList<StringValue>"]
			sequence<StringValue> StringSeq;
	
			["java:type:java.util.ArrayList<FloatValue>"]
			sequence<FloatValue> FloatSeq;
	
			["java:type:java.util.ArrayList<DoubleValue>"]
			sequence<DoubleValue> DoubleSeq;
	
			["java:type:java.util.ArrayList<Algorithm>"]
			sequence<Algorithm> AlgorithmSeq;
	
			["java:type:java.util.ArrayList<AlgorithmInputData>"]
			sequence<AlgorithmInputData> AlgorithmInputDataSeq;
	
			["java:type:java.util.ArrayList<AlgorithmOutput>"]
			sequence<AlgorithmOutput> AlgorithmOutputSeq;
	
			["java:type:java.util.ArrayList<AlgorithmInputInt"] 
			sequence<AlgorithmInputInt> AlgorithmInputIntSeq;
	
			["java:type:java.util.ArrayList<AlgorithmInputReal>"]
			sequence<AlgorithmInputReal> AlgorithmInputRealSeq;
	
			["java:type:java.util.ArrayList<AlgorithmInputChar>"]
			sequence<AlgorithmInputChar> AlgorithmInputCharSeq;
	
			["java:type:java.util.ArrayList<AlgorithmModel>"]
			sequence<AlgorithmModel> AlgorithmModelSeq;
	
			["java:type:java.util.ArrayList<LogicalConnector>"]
			sequence<LogicalConnector> LogicalConnectorSeq;
	
			["java:type:java.util.ArrayList<Parameter>"]
			sequence<Parameter> ParameterSeq;
	
			["java:type:java.util.ArrayList<DataSource>"]  
			sequence<DataSource> DataSourceSeq;
	
			["java:type:java.util.ArrayList<Transducer>"]
			sequence<Transducer> TransducerSeq;
	
			["java:type:java.util.ArrayList<ModuleRef>"] 
    		sequence<ModuleRef> ModuleRefSeq;
    
   	 		["java:type:java.util.ArrayList<OutPortSet>"]
			sequence<OutPortSet> OutPortSetSeq;
	
			["java:type:java.util.ArrayList<ModuleDescriptor>"]
			sequence<ModuleDescriptor> ModuleDescriptorSeq;
	
			["java:type:java.util.ArrayList<NumAlert>"]
			sequence<NumAlert> NumAlertSeq;
	
			["java:type:java.util.ArrayList<AlertRegion>"]
    		sequence<AlertRegion> AlertRegionSeq;
    
    		["java:type:java.util.ArrayList<Data>"]  
			sequence<Data> DataSeq;
	
			["java:type:java.util.ArrayList<DataEvent>"]  
			sequence<DataEvent> DataEventSeq;
	
			["java:type:java.util.ArrayList<Value>"]
			sequence<Value> ValueSeq;
	
			["java:type:java.util.ArrayList<DataEventSet>"]  
			sequence<DataEventSet> DataEventSetSeq;
	
			["java:type:java.util.ArrayList<DataRef>"]
			sequence<DataRef> DataRefSeq;
	
			["java:type:java.util.ArrayList<ExplanationDataRef>"]
			sequence<ExplanationDataRef> ExplanationDataRefSeq;
	
			["java:type:java.util.ArrayList<ExplanationData>"]
			sequence<ExplanationData> ExplanationDataSeq;
	
			["java:type:java.util.ArrayList<EntryPointStringfield>"]
			sequence<EntryPointStringfield> EntryPointStringfieldSeq;
	
			["java:type:java.util.ArrayList<ItemHealth>"]
			sequence<ItemHealth> ItemHealthSeq;
	
			["java:type:java.util.ArrayList<ItemEventNumInt>"]
			sequence<ItemEventNumInt> ItemEventNumIntSeq;
	
			["java:type:java.util.ArrayList<ItemEventChar>"]
			sequence<ItemEventChar> ItemEventCharSeq;
	
			["java:type:java.util.ArrayList<ItemEventNumReal>"]
			sequence<ItemEventNumReal> ItemEventNumRealSeq;
	
			["java:type:java.util.ArrayList<ItemEventBLOB>"]
			sequence<ItemEventBLOB> ItemEventBLOBSeq;
	
			["java:type:java.util.ArrayList<ItemRecommendationRemark>"]
			sequence<ItemRecommendationRemark> ItemRecommendationRemarkSeq;
	
			["java:type:java.util.ArrayList<Metric>"]
			sequence<Metric> MetricSeq;
	
			["java:type:java.util.ArrayList<LongValue>"]  
			sequence<LongValue> LongSeq;
	
			["java:type:java.util.ArrayList<PortRef>"]
			sequence<PortRef> PortRefSeq;
	
			["java:type:java.util.ArrayList<Role>"]   
			sequence<Role> RoleSeq;
	
			["java:type:java.util.ArrayList<SelectionFilter>"]  
			sequence<SelectionFilter> SelectionFilterSeq;
	
			["java:type:java.util.ArrayList<MonitorId>"]
			sequence<MonitorId> MonitorIdSeq;
	
			["java:type:java.util.ArrayList<MonitorIdGroup>"] 
			sequence<MonitorIdGroup> MonitorIdGroupSeq;
	
			["java:type:java.util.ArrayList<ItemPrognosis>"]
			sequence<ItemPrognosis> ItemPrognosisSeq;
	
			["java:type:java.util.ArrayList<Port>"]
			sequence<Port> PortSeq;
	
			["java:type:java.util.ArrayList<MIMKey3>"]  
			sequence<MIMKey3> MIMKey3Seq;
	
			["java:type:java.util.ArrayList<Function>"]
			sequence<Function> FunctionSeq;
	
			["java:type:java.util.ArrayList<SDEnumSetDataItem>"]
			sequence<SDEnumSetDataItem> SDEnumSetDataItemSeq;
	
			["java:type:java.util.ArrayList<ItemEvent>"]
			sequence<ItemEvent> ItemEventSeq;
	
			["java:type:java.util.ArrayList<ItemEventConfig>"]
			sequence<ItemEventConfig> ItemEventConfigSeq;
	
			["java:type:java.util.ArrayList<MIMAgent>"]
			sequence<MIMAgent> MIMAgentSeq;
	
			["java:type:java.util.ArrayList<Item>"]
			sequence<Item> ItemSeq;
	
			enum SiteCategory {
				SiteZero, 
				SiteEntZero, 
				SitePlat, 
				SiteTemplate, 
				SiteSpecific, 
				SiteZeroOne, 
				SiteZeroTwo, 
				SitPlatTrngData
    		};
				  
			["java:getset"]
			class Site {
				["protected"] SiteCategory category;
				["protected"] string siteId;
				["protected"] string regId;
				["protected"] string userTag;
				["protected"] string systemUserTag;
			};			

			["java:getset"]
			class Parameter {
        		["protected"] string description;
			};

			["java:getset"]
			class ErrorInfo {
    			["protected"] ParameterSeq parameters;   
			};

			["java:getset"]
			class EntryPointStringfield {
				["protected"] string epStr;
			};

			enum OsacbmTimeType {
				OsacbmTimeMimosa,
				OsacbmTimePosixNSec8,
				OsacbmTimePosixUSec8,
				OsacbmTimePosixUSec6,
				OsacbmTimePosixMSec6,
				OsacbmTimePosixSec4,
				OsacbmTimeTickNSec,
				OsacbmTimeTickUSec,
				OsacbmTimeTickMSec,
				OsacbmTimeSystemTick                 
			};
	
			["java:getset"]				
			class LocalTime {
				["protected"] IntValue hourDelta;
				["protected"] IntValue minDelta;
				["protected"] BooleanValue dst;
    		};
	
			["java:getset"]
			class OsacbmTime {
				["protected"] LocalTime localTimeValue;
				["protected"] OsacbmTimeType timeType;
				["protected"] string time;
				["protected"] BigIntValue timeBinary;			
    		};		
	
			["java:getset"]
			class DataEvent {
				["protected"] LongValue id;
				["protected"] Site siteValue;
				["protected"] FloatValue confid;
				["protected"] OsacbmTime time;
				["protected"] LongValue sequenceNum;
				["protected"] BooleanValue alertStatus;			   
			};		

			["java:getset"]
			class DataEventSet {
				["protected"] Site siteValue;
				["protected"] LongValue id;
				["protected"] OsacbmTime time;
				["protected"] LongValue sequenceNum;
				["protected"] BooleanValue alertStatus;
				["protected"] DataEventSeq dataEvents;
    		};

			enum AlertFilterSetting { 
				AllEvents,
				AlertsEventsOnly,
				NoEvents,
				ChangedValueEvents
			};
	
			["java:getset"]
			class SequenceSelect {
				["protected"] IntValue sequenceNumStart;
				["protected"] IntValue sequenceNumEnd;
			};

			["java:getset"]
			class TimeSelect {
				["protected"] OsacbmTime timeStart;
				["protected"] OsacbmTime timeEnd;
				["protected"] IntValue snapshotStart;
				["protected"] IntValue snapshotEnd;
				["protected"] IntValue decimate;
			};

	enum DataIdType { 
		measLocation, 
		agentId 
	};

	enum SegOrAs {
		SegmentValue,
		AssetValue
	};

	["java:getset"]
	class ItemId {
  		["protected"] SegOrAs segOrAsValue;
	    ["protected"] Site siteValue;
	    ["protected"] IntValue code;
	    ["protected"] string userTag;
	    ["protected"] string name;
	};

	["java:getset"]
	class SelectionFilter {
	
    };

	["java:getset"]
	class MonitorId {
		["protected"] Site siteValue;
		["protected"] IntValue id;
		["protected"] DataIdType type;
		["protected"] ItemId itemIdValue;
		["protected"] SelectionFilterSeq selectionFilterValue;
	};

	["java:getset"]
	class MonitorIdGroup {
		 ["protected"] Site siteValue;
         ["protected"] IntValue dataEventSetId;
         ["protected"] AlertFilterSetting alertFilter;
         ["protected"] SequenceSelect sequenceSelectValue;
         ["protected"] TimeSelect timeSelectValue;
         ["protected"] MonitorIdSeq monitorIdValue;
    };

	["java:getset"]
	class MIMKey2 {
		["protected"] Site siteValue;
		["protected"] LongValue id;
		["protected"] string name;
	};

	enum ChannelType { 
		ModuleFault,
		RtnOnRequest,
		RtnAll,
		RtnAlerts
	};

	enum OsacbmDataType {
                    InvalidUnsetType,DAWaveformType,DABLOBDataType,DAType,DADataSeqType,
					DARealType,DAIntType,DABoolType,DMAmplType,DMType,DMVectorType,DMRealFrqSpectType,DMCmplxFrqSpectType,
					DMCPBType,DMRealType,DMIntType,DMBoolType,DMRealWaveformType,DMCmplxWaveformType,DMDataSeqType,
					DMUserDefv,SDRealType,SDIntType,SDBoolType,DEnumType,HADataEventType,PADataEventType,PARULType,
					PARULDistType,PAFutureHlthType,PAFutureHlthTrendType,AGDataEventType,DAUnknownType,
					DMUnknownType,SDUnknownType,HAUnknownType,PAUnknownType,AGUnknownType,DAInvalidType,
					DMInvalidType,SDInvalidType,HAInvalidType,PAInvalidType,AGInvalidType,DEUserType,
					SDEnumSetType,SDTestRealType,SDTestIntType,DMBLOBDataType,DAStringType,SDEventType,
					DAValueWaveformType,DAValueDataSeqType};

	["java:getset"]
	class Port {
		["protected"] LongValue id;
		["protected"] OsacbmTime lastUpdate;
		["protected"] string config;
		["protected"] string name;
		["protected"] string userTag;
		["protected"] OsacbmDataType osacbmDataTypeValue;
		["protected"] Site siteValue;
	};

	["java:getset"]
	class PortRef {
		["protected"] MonitorId monitorIdValue;
		["protected"] Port inPort;
	 };

	["java:getset"]
	class ModuleRef {
		["protected"] MIMKey2 modId;
		["protected"] ChannelType chType;
	    ["protected"] PortRefSeq inportRefs; 
     };
	
	["java:getset"]
	class MIMKey3 {
		["protected"] Site siteValue;
		["protected"] LongValue dbid;
	    ["protected"] LongValue code;
		["protected"] string name;
	};

	["java:getset"]
	class UnitConverter {
		["protected"] DoubleValue multiplier;
	    ["protected"] DoubleValue offset;
	};

	["java:getset"]
	class MIMKey1 {
		["protected"] LongValue code;
	    ["protected"] string name;
	};

	["java:getset"]
	class RefUnit {
		["protected"] MIMKey1 id;
    };

	["java:getset"]
	class EngUnit {
 		["protected"] Site siteValue;
		["protected"] IntValue dbid;
		["protected"] IntValue code;
		["protected"] string name;
		["protected"] string abbrev;
		["protected"] UnitConverter unitConv;
		["protected"] RefUnit refUnitValue;
	 };

	["java:getset"]
	class AlgorithmInputData {
		["protected"] LongValue argId;
		["protected"] MonitorId inputRef;
		["protected"] string name;
		["protected"] string userTag;
		["protected"] string desc;
		["protected"] EngUnit expectedEu;
		["protected"] OsacbmDataType expectedDataType;
		["protected"] MIMKey3 dataContentType;
	};

	["java:getset"]
	class AlgorithmOutput {
	   ["protected"] LongValue argId;
	   ["protected"] OsacbmTime startTime;
  	   ["protected"] string name;
       ["protected"] string userTag;
       ["protected"] string desc;
       ["protected"] EngUnit outputEu;
       ["protected"] MonitorId outputRef;
   };

   ["java:getset"]
   class AlgorithmInputInt {
	   ["protected"] LongValue argId;
       ["protected"] IntValue value;
	   ["protected"] string name;
	   ["protected"] string userTag;
	   ["protected"] string desc;
	   ["protected"] EngUnit eu;
	   ["protected"] OsacbmTime lastUpdate;
	   ["protected"] BooleanValue constant;
	 };

	["java:getset"]
	class AlgorithmInputReal {
		["protected"] LongValue argId;
	    ["protected"] DoubleValue value;
	    ["protected"] string userTag;
	    ["protected"] string desc;
	    ["protected"] EngUnit eu;
	    ["protected"] OsacbmTime lastUpdate;
	    ["protected"] BooleanValue constant;
	};
	
	["java:getset"]
	class AlgorithmInputChar{
		["protected"] LongValue argId;
		["protected"] string value;
		["protected"] string name;
		["protected"] string userTag;
		["protected"] string desc;
		["protected"] EngUnit eu;
		["protected"] OsacbmTime lastUpdate;
		["protected"] BooleanValue constant;
	};
	
	["java:getset"] 
	class AlgorithmModel{
		["protected"] MIMKey3 compModelId;
	 	["protected"] LongValue algModelId;
		["protected"] string algNameForModel;
		["protected"] string name;
		["protected"] string userTag;
		["protected"] string desc;
		["protected"] OsacbmTime lastUpdate;
		["protected"] MIMKey3 manufacturer;
		["protected"] string version;
	};

	["java:getset"]
	class OutPortSet {
		["protected"] Site siteValue;
	    ["protected"] LongValue id;
	    ["protected"] LongValue verId;
	    ["protected"] Site templateSite;
	    ["protected"] OsacbmTime lastUpdate;
        ["protected"] PortSeq outPorts; 
	 };

	["java:getset"]
	class Algorithm {
		["protected"] MIMKey3 id;
		["protected"] IntValue verNum;
		["protected"] OsacbmTime startTime;
		["protected"] MIMKey3 algorithmType;
		["protected"] string userTag;
		["protected"] string name;
		["protected"] string description;
		["protected"] string URIprocessDesc;
		["protected"] MIMKey3 URIaprocType;
		["protected"] MIMKey3 URIbdType;
		["protected"] string processDesc;
	    ["protected"] ByteSeq processDescBinary;
		["protected"] MIMKey3 algProcType;
		["protected"] MIMKey3 procBdType;
		["protected"] AlgorithmInputDataSeq inputData;
		["protected"] AlgorithmOutputSeq algorithmOutputs;
		["protected"] AlgorithmInputIntSeq inputInts;
		["protected"] AlgorithmInputRealSeq inputReals;
		["protected"] AlgorithmInputCharSeq inputChars;
		["protected"] AlgorithmModelSeq models;
    };

	["java:getset"]
	class ModuleDescriptor {
		["protected"] Site modIdSite;
		["protected"] IntValue modIdCode;
		["protected"] string modTag;
		["protected"] string modName;
		["protected"] string description;
		["protected"] string version;
	};

	["java:getset"]
	class Item {
		["protected"] MIMKey2 id;
    	["protected"] string userTag;
	};

	["java:getset"]
	class Function {
		["protected"] ItemId itemIdValue;
        ["protected"] Site funcDbSite;
        ["protected"] LongValue funcDbId;
        ["protected"] LongValue seq;
        ["protected"] string userTag;
        ["protected"] string name;
	    ["protected"] MIMKey2 byAgent;
    };

    ["java:getset"]
    class Role {
	    ["protected"] MIMKey2 id;
	    ["protected"] MIMKey3 agentRoleType;
    };

	["java:getset"]
	class MIMAgent {
		["protected"] Site agentSite;
	    ["protected"] LongValue agentId;
		["protected"] MIMKey3 agentType;
		["protected"] string name;
		["protected"] PortRefSeq inputs;
	    ["protected"] PortRefSeq outputs;
		["protected"] RoleSeq roles;
	};

	["java:getset"]
	class SupportingData {
		["protected"] ItemSeq items;
	    ["protected"] FunctionSeq funcs;
	    ["protected"] MIMAgentSeq agents;
	 };

	["java:getset"]
	class Configuration {
		["protected"] ModuleRefSeq inportModuleSet;
		["protected"] AlgorithmSeq algorithms;
		["protected"] OutPortSetSeq outPortSets;
		["protected"] ModuleDescriptorSeq moduleIds;
		["protected"] SupportingData supportingDataValue;
	};

	["java:getset"]
	class ConfigRequest {
		["protected"] BooleanValue rtnAll;
		["protected"] BooleanValue rtnModDesc;
		["protected"] BooleanValue rtnConfigRequest;
		["protected"] MonitorIdGroup mList;
		["protected"] string specialReq;
   	};

	["java:getset"]
   	class Explanation {
		["protected"] BooleanValue used;
    };

	["java:getset"]
	class DataRef {
		["protected"] Site siteValue;
	    ["protected"] LongValue id;
		["protected"] DataIdType dataIdTypeVal;
		["protected"] OsacbmTime time;
		["protected"] OsacbmDataType dataType;
		["protected"] ItemId itemIdValue;
    };

	["java:getset"]
    class ExplanationData {
		["protected"] MIMKey3 explType;
		["protected"] string explTypeDesc;
		["protected"] DataRef resultDataRef;
		["protected"] DataEventSetSeq explDataEventSets;
	};

    ["java:getset"]
	class ExplanationDataSet extends Explanation {
    	["protected"] ExplanationDataSeq explData;
    };

    ["java:getset"]
	class ExplanationDataRef {
		["protected"] MIMKey3 explType;
		["protected"] string explTypeDesc;
		["protected"] DataRef resultDataRef;
		["protected"] DataRefSeq explDataRefSrcs;
	};

	["java:getset"]
    class ExplanationDataRefSet extends Explanation {
    	["protected"] ExplanationDataRefSeq explDataRefs;
    };

    ["java:getset"]
    class ExplanationSrcs extends Explanation { 
    
    };

	["java:getset"]
    class EntryPoIntValueStringified {
		["protected"] string epStr;
	};

	["java:getset"]
    class ExplanationSrcsStr extends Explanation {
		["protected"] EntryPointStringfieldSeq strEntryPoIntValues; 
    };

    ["java:getset"]
    class ControlChange {
		["protected"] ParameterSeq parameters;   
    };

	["java:getset"]
    class ControlRequest {
		["protected"] ParameterSeq parameters;   
    };

	["java:getset"]
    class ControlInfo {
		["protected"] ParameterSeq parameters;   
    };

	["java:getset"]
    class AppChange {
		["protected"] ParameterSeq parameters;   
	};

	["java:getset"]
	class AppInfo {
		["protected"] ParameterSeq parameters;  
	};

	["java:getset"]
	class AppRequest {
		["protected"] ParameterSeq parameters;  
	};

	["java:getset"]
	class ErrorRequest {
		["protected"] ParameterSeq parameters;   
	};

	["java:getset"]
	class MonitorIdGroupList {
		["protected"] MonitorIdGroupSeq monitorIdGroupValue;
	};

	["java:getset"]
	class ItemRequestForWork {
		["protected"] MIMKey3 requestId;
		["protected"] MIMKey3 workManageType;
		["protected"] MIMKey3 workTaskType;
		["protected"] string autoApprove;
		["protected"] OsacbmTime startBefore;
		["protected"] OsacbmTime endBefore;
		["protected"] OsacbmTime startAfter;
		["protected"] OsacbmTime endAfter;
  	    ["protected"] LongValue repeatInterval;
		["protected"] EngUnit intervalEU;
		["protected"] MIMAgent to;
		["protected"] MIMKey3 solutionPackage;
		["protected"] ItemId recSegment;
		["protected"] OsacbmTime recGmt;
		["protected"] MIMAgent recBy;   
		["protected"] MIMKey3 workRequest;
		["protected"] MIMKey3 workOrder;
		["protected"] string name;
		["protected"] string longDescription;
	};

	["java:getset"]
	class ItemRecommendationRemark {
		["protected"] string remarkText;
		["protected"] LongValue ordSeq;
	};

	["java:getset"]
	class ItemRecommendation {
		["protected"] ItemId itemIdValue;
		["protected"] OsacbmTime utcRecommendation;
		["protected"] MIMAgent by;
		["protected"] MIMKey3 priorityTypeCode;
		["protected"] string userTag;
		["protected"] string name;
		["protected"] ItemRequestForWork itemRequestForWorkVal;
		["protected"] ItemRecommendationRemarkSeq itemRecommendationRemarks;
    };

	["java:getset"]
	class AGDataEvent extends DataEvent {
        ["protected"] ItemRecommendationSeq itemRecommendations;
    };

	["java:getset"]
	class AgentAlertFilter extends SelectionFilter {
		["protected"] ItemId itemIdValue;
		["protected"] MIMKey3 eventType;
        ["protected"] MIMKey3 severityType;
      };

	["java:getset"]
	class AGPort extends Port {
		["protected"] MIMAgent by;
	};

	["java:getset"]
	class AlertRegionRef {
		["protected"] Site regionSite;
		["protected"] LongValue regionId;
		["protected"] OsacbmTime regionLastUpdate;
		["protected"] LongValue regionSeq;
	};

	["java:getset"]
	class AlertType {
	    ["protected"] Site alertTypeSite;
	    ["protected"] LongValue alertTypeId;
	    ["protected"] LongValue alertTypeCode;
	    ["protected"] MIMKey3 alertSeverity;
  	    ["protected"] string alertName;
    };

	["java:getset"]
	class AlertRegion {
		["protected"] AlertRegionRef regionRef;
		["protected"] AlertType alertTypeValue;
		["protected"] string regionName;
	};
	
	["java:getset"]
	class EnumValue {
		["protected"] IntValue value;
		["protected"] string name;
		["protected"] EngUnit enumEU;
	};
	
	["java:getset"]
	class AlertRegionCBM extends AlertRegion {
		["protected"] DoubleValue minAmpl;
		["protected"] BooleanValue minInclusive;
		["protected"] DoubleValue maxAmpl;
		["protected"] BooleanValue maxInclusive;
		["protected"] EngUnit amplEU;
		["protected"] EnumValue regionEnum;
		["protected"] DoubleValue minAmplHysteresis;
		["protected"] DoubleValue maxAmplHysteresis;
		["protected"] EngUnit hysteresisEU;
		["protected"] DoubleValue bandDelay;
		["protected"] EngUnit bandDelayEU;
		["protected"] MIMKey3 measLocType;
		["protected"] MIMKey3 mlocCalcType;  
		["protected"] BooleanValue hiLowSideUsed;
	};

	["java:getset"]
	class Metric {
		["protected"] MIMKey3 metricId;
	    ["protected"] FloatValue value;
	    ["protected"] EngUnit metricEU;
    };

	["java:getset"]
	class LogicalConnector {
		["protected"] DoubleValue likelihood;
		["protected"] IntValue nodeId;
		["protected"] Explanation  nodeEventExplanation;
		["protected"] MetricSeq metrics; 
	};

	enum DataStatus {
    	Ok,
		Failed,
		Unknown,
		NotUsed
    };

	["java:getset"]
    class NumAlert {
		["protected"] Site alertTypeSite;
		["protected"] LongValue alertTypeId;
		["protected"] LongValue alertTypeCode;
		["protected"] BooleanValue hiSideAlert;
		["protected"] OsacbmTime lastTrigger;
		["protected"] MIMKey3 alertSeverity;
		["protected"] string alertName;
		["protected"] AlertRegionRef regionRef;
		["protected"]EnumValue regionEnum;
	 };
	
	["java:getset"]
	class DMDataEvent extends DataEvent {
		["protected"] DataStatus  dataStatusValue;
		["protected"] NumAlertSeq    numAlerts; 
	};

	["java:getset"]
	class Ampl extends DMDataEvent {
		["protected"] DoubleValue phase;
		["protected"] DoubleValue value;
	};

	["java:getset"]
	class AmbiguityGroup {
		["protected"] MIMKey3 ambId;
		["protected"] OsacbmTime estStart;
		["protected"] string userTag;
		["protected"] string name;
		["protected"] MIMKey3 ambiguityType;
		["protected"] LogicalConnector logConnector;
   };

	["java:getset"]
    class AndConnector extends LogicalConnector {
    	["protected"] LogicalConnectorSeq conjuncts;
	};

	["java:getset"]
	class DataSource {
	    ["protected"] MIMKey3 dataSrcType;
	};

	["java:getset"]	
	class Transducer {
		["protected"] MIMKey3 transdType;
		["protected"] EngUnit outEU;
		["protected"] DoubleValue outAmpl;
		["protected"] EngUnit calibEU;
		["protected"] OsacbmTime lastCalib;
		["protected"] BooleanValue selfPowered;
		["protected"] string name;
	};

	["java:getset"]
	class AssetInfo {
		["protected"] DataSourceSeq dataSources;
	    ["protected"] TransducerSeq transd;
    };

	["java:getset"]
	class Asset extends Item {
		["protected"] string serialNo;
		["protected"] MIMKey3 assetType;
		["protected"] AssetInfo assetInfoValue;
	};

	["java:getset"]
	class Mime {
		["protected"] string value;
	};
	
	["java:getset"]
	class BLOB {
		["protected"] ByteSeq data;
	    ["protected"] Mime contentType;
	};

	enum BndType {
		percent,
		octave  
    };


	["java:getset"]
    class Value {

	};
   
    ["java:getset"]
	class BooleanArrayValue extends Value {
		["protected"] BoolSeq values;
	};

	["java:getset"]
	class BooleanValue extends Value {
		["protected"] bool value;
	};

	["java:getset"]
	class ByteArrayValue extends Value {
		["protected"] ByteSeq values;
	};

	["java:getset"]
	class ByteValue extends Value {
	    ["protected"] byte value;
    };
    
    ["java:getset"]
	class CharArrayValue extends Value {
	    ["protected"] StringSeq values;
	};

	["java:getset"]
	class CharValue extends Value {
	    ["protected"] string value;
    };
	
	["java:getset"]
	class ComplexValue extends Value {
		["protected"] DoubleValue realValue;
	 	["protected"] DoubleValue imagValue;
	};
	
	["java:getset"]
	class CmplxArrayValue extends Value {
		["protected"] DoubleSeq realValues;
		["protected"] DoubleSeq imagValues;
	};

	["java:getset"] 
	class CmplxFrqSpect extends DMDataEvent {
		["protected"] DoubleValue xAxisMin;
        ["protected"] DoubleValue xAxisDelta;
		["protected"] DoubleSeq realValues;
		["protected"] DoubleSeq imagValues;
	};

	["java:getset"]
	class CmplxWaveform extends DMDataEvent {
	    ["protected"] DoubleValue xAxisStart;
        ["protected"] DoubleValue xAxisDelta;
		["protected"] DoubleSeq realValues;
		["protected"] DoubleSeq imagValues;
	};

	["java:getset"]
	class CPB {
		["protected"] BndType cpbBndType;
		["protected"] DoubleValue  cntrBnd1Hz;
		["protected"] DoubleValue  bndWidth;
		["protected"] DoubleSeq values;
	};

	["java:getset"]
	class DADataEvent extends DataEvent {
		["protected"] DataStatus dataStatusValue;
		["protected"] NumAlertSeq numAlerts; 
    };
   
    ["java:getset"]
	class DABLOBData extends DADataEvent {
		["protected"] MIMKey3 mEventBlobType;
		["protected"] BLOB value;
    };

	["java:getset"]
	class DABool extends DADataEvent {
		["protected"] BooleanValue value;
    };

	["java:getset"]
    class DADataSeq extends DADataEvent {
		["protected"] DoubleValue xAxisStart;
		["protected"] DoubleSeq xAxisDeltas;
		["protected"] DoubleSeq values;
    };

	["java:getset"]
    class DAInt extends DADataEvent {
		["protected"] IntValue value;
    };

  	["java:getset"]
	class MeasLoc {
		["protected"] MIMKey2 measLocId;
		["protected"] MIMKey3 measLocType;
		["protected"] string name;
		["protected"] string userTag;
	};

	["java:getset"]
	class DAPort extends Port {
		["protected"] EngUnit valueEU;
		["protected"] EngUnit xAxisEU;
		["protected"] MeasLoc measLocVal;
		["protected"] AlertRegionSeq alertRegs;
	};

	["java:getset"]  
	class DAReal extends DADataEvent {
       ["protected"] DoubleValue value;
	};

	["java:getset"]
	class DAString extends DADataEvent {
		["protected"] string value;
	};

	["java:getset"]
    class DataType {
	    ["protected"] MIMKey3 id;
	    ["protected"] EngUnit dataTypeValue;
    };
   
	["java:getset"]
    class Data {
		["protected"] OsacbmTime time;
	    ["protected"] DataType dataTypeVal;
	    ["protected"] Value valueVal;
	    ["protected"] DataSeq  compositeData;
	};

	["java:getset"]
	class DataSourceMeasInfo {
		["protected"] MIMKey3 dataSrcType;
		["protected"] MIMKey3 mLocCalcType;
		["protected"] FloatValue mCalcSize;
	};

	["java:getset"]
	class DAValueDataSeq extends DADataEvent {
		["protected"] Value xAxisStart;
		["protected"] ValueSeq xAxisDeltas;
		["protected"] ValueSeq values;
	};

	["java:getset"]
	class DAValueWaveform extends DADataEvent {

			["protected"] Value xAxisStart;
			["protected"] Value xAxisDelta;
			["protected"] ValueSeq values;
	  };

	["java:getset"]
	class DA extends DADataEvent {
		["protected"] DoubleValue xValue;
		["protected"] DoubleValue value;
	};

	["java:getset"]  
  	class DAWaveform extends DADataEvent {
		["protected"] DoubleValue xAxisStart;
	    ["protected"] DoubleValue xAxisDelta;
	    ["protected"] DoubleSeq values;
    };

	["java:getset"]   
	class DblArrayValue extends Value {
		["protected"] DoubleSeq values;
	};

	["java:getset"] 
	class DMBLOBData {
		["protected"] MIMKey3 mEventBlobType;
		["protected"] BLOB value;
    };

	["java:getset"] 
	class DMBool extends DMDataEvent {
		["protected"] BooleanValue value; 
	};

	["java:getset"]
	class DMDataSeq extends DMDataEvent {
		["protected"] DoubleValue xAxisStart;
		["protected"] DoubleSeq xAxisDeltas;
		["protected"] DoubleSeq values;
	};

	["java:getset"]
	class DMInt extends  DMDataEvent {
		["protected"] IntValue value; 
	};

	["java:getset"]
	class WindowType {
		["protected"] MIMKey3 id;
		["protected"] DoubleValue pfMultiplier;
	};

	["java:getset"]
	class MimTypeDescriptors {
		["protected"] MIMKey3 postScalType;
		["protected"] WindowType windowTypeValue;
		["protected"] MIMKey3 srcDetectType;
		["protected"] DoubleValue xAxisMax;
		["protected"] DoubleValue xAxisMin;
	};

	["java:getset"]
	class DMPort extends Port {
		["protected"] EngUnit valueEU;
		["protected"] EngUnit xAxisEU;
		["protected"] EngUnit phaseEU;
		["protected"] AlertRegionSeq alertRegs;
		["protected"] MeasLoc measLocVal;
		["protected"] MimTypeDescriptors types;
	};

	["java:getset"]
	class DMReal extends DMDataEvent {
		["protected"] DoubleValue value; 
	};

	["java:getset"]
	class DMVector extends DMDataEvent {
        ["protected"] DoubleValue xValue;
	    ["protected"] DoubleValue value;
    };

	["java:getset"]
    class DoubleValue extends Value {
		["protected"] double value;
	};

	["java:getset"]
	class EPSStatus {
		["protected"] string status;
	};

	["java:getset"]
   	class FloatArrayValue extends Value {
		["protected"] FloatSeq values;
    };

	["java:getset"]
    class FloatValue extends Value {
		["protected"] float value;
    };

    ["java:getset"]
	class ItemPrognosis {
		["protected"] ItemId itemidValue;
		["protected"] OsacbmTime estUTC;
	};

	["java:getset"]
    class FutureHealth extends ItemPrognosis {
		["protected"] DoubleValue hlthGrade;
		["protected"] DoubleValue atRef;
		["protected"] DoubleValue error;
		["protected"] DoubleValue postConfid;
	};

	["java:getset"]
	class FutureHlthTrend extends ItemPrognosis {
		["protected"] DoubleSeq hlthGrades;
		["protected"] DoubleSeq atRefs;
		["protected"] DoubleSeq errors;
		["protected"] DoubleSeq postConfids;
	};

	enum XmlDataType { 
		BooleanType,
		DataTimeType,
		DecimalType,
		DoubleType,
		FloatType,
		IntegerType,
		StringType   
	};
	
	["java:getset"]    
	class GeneralParameter extends Parameter {
		["protected"] XmlDataType dataType;
	    ["protected"] string name;
	    ["protected"] string value;
	};

	["java:getset"]
	class HlthLevelType {
        ["protected"] MIMKey3 id;
		["protected"] ShortValue healthScale;
	};

	["java:getset"]
	class ItemHealth {
		["protected"] ItemId itemIdValue;
		["protected"] OsacbmTime utcHealth;
		["protected"] HlthLevelType healthLevelType;
		["protected"] LongValue hLevel;
		["protected"] DoubleValue hGradeReal;
		["protected"] DoubleValue likelihood;
		["protected"] MIMKey3 chgPattType;
	};

	["java:getset"]
	class HADataEvent extends DataEvent {
		["protected"] BooleanValue healthGood;
		["protected"] AmbiguityGroup diagnosis;
		["protected"] ItemHealthSeq itemHealths; 
	};

	["java:getset"]
	class HAPort extends Port {
		["protected"] MIMAgent by;
		["protected"] EngUnit enumEU;
	};

	["java:getset"]
	class IntArrayValue extends Value {
		["protected"] IntSeq values;
	};

	["java:getset"]
	class IntValue extends Value {
		["protected"] int value;
	};

	["java:getset"] 
	class ItemAlertRegions {
		["protected"] AlertRegionSeq alertRegs;
		["protected"] ItemId itemIdValue;
		["protected"] MIMKey3 monitorType;
	};

	["java:getset"]
	class ItemEventBLOB {
		["protected"] MIMKey3 blobDataType;
	    ["protected"] LongValue ordSeq;
		["protected"] MIMKey3 blobContentType;
		["protected"] string userTag;
		["protected"] string name;
		["protected"] string assocFileName;
		["protected"] BLOB imageData;  
	};

	["java:getset"]
	class ItemEventConfig {
	    ["protected"] ItemId itemIdValue;
	    ["protected"] MIMKey3 eventType;
	    ["protected"] string name;
	    ["protected"] string userTag;
	};

	["java:getset"]
	class ItemEventChar {
		["protected"] MIMKey3 evCharDataType;
		["protected"] EngUnit engUnitValue;
		["protected"] string dataValue;
	};

	["java:getset"]
	class ItemEventNumInt {
		["protected"] MIMKey3 evNumDataType;
		["protected"] EngUnit engUnitValue;
		["protected"] IntValue dataValue;
	};

	["java:getset"]
	class ItemEventNumReal {
		["protected"] MIMKey3 evNumDataType;
		["protected"] EngUnit engUnitValue;
		["protected"] DoubleValue dataValue;
	};
	
	["java:getset"]
	class ItemEvent {
		["protected"] ItemId itemIdValue;
		["protected"] MIMKey3 eventType;
		["protected"] OsacbmTime eventStart;
		["protected"] OsacbmTime eventStop;
		["protected"] string name;
		["protected"] string userTag;
		["protected"] ItemEventNumIntSeq itemEventNumIntVal;
		["protected"] ItemEventCharSeq itemEventCharValue;
		["protected"] ItemEventNumRealSeq itemEventNumRealVal;
		["protected"] ItemEventBLOBSeq itemEventBLOBVal;
	};

	["java:getset"]
	class LongArrayValue extends Value {
        ["protected"] LongSeq values;
    };

	["java:getset"]
	class LongValue extends Value {
		["protected"] long value;
	};
	
	["java:getset"]
	class BigIntValue extends Value {
		["protected"] long value;
	};

	["java:getset"]
	class TransducerMeasInfo {
		["protected"] MIMKey3 transdType;
		["protected"] IntValue taOrientDeg;
	    ["protected"] MIMKey3 trAxDirType;
		["protected"] LongValue mimLocSeq;
		["protected"] string motionDirection;
	};

	["java:getset"]
	class MeasLocMIM extends MeasLoc {
     	["protected"] SegOrAs segOrAsValue;
		["protected"] MIMKey2 segment;  
		["protected"] MIMKey2 asset;  
		["protected"] string mimUserPrefix;  
	 	["protected"] DoubleValue updateIntValueerval;  
		["protected"] EngUnit updateEU;  
		["protected"] IntValue collectDuration;  
		["protected"] EngUnit collectEU;  
		["protected"] string barCode;  
		["protected"] DataSourceMeasInfo dataSrc;
		["protected"] TransducerMeasInfo transd;
	};

	["java:getset"]
	class MeasLocAlertFilter extends SelectionFilter {
		["protected"] Site alertTypeSite;
		["protected"] LongValue alertTypeId;
		["protected"] LongValue alertTypeCode;
		["protected"] MIMKey3 alertSeverity;
		["protected"] AlertRegionRef regionRef; 
		["protected"] EnumValue regionEnum;
	};

	["java:getset"]
	class NotConnector extends LogicalConnector {
		["protected"] LogicalConnector notArg;
	};  

	["java:getset"]
	class OrConnector extends LogicalConnector {
		["protected"] LogicalConnectorSeq disjuncts;
	};

	["java:getset"]
	class PADataEvent extends DataEvent {
		["protected"] AmbiguityGroup prognosis;
		["protected"] ItemPrognosisSeq itemPrognoses;
	};

	["java:getset"]
	class PAPort extends Port {
		["protected"] MIMAgent by;
		["protected"] EngUnit refOrRulEU;
    };

	["java:getset"]
    class PropEvent extends LogicalConnector {
    	["protected"] ItemId itemIdValue;
		["protected"] MIMKey3 eventType;
		["protected"] OsacbmTime estStart;
		["protected"] OsacbmTime estEnd;
		["protected"] MIMKey3 severutyType;
		["protected"] IntValue critically;
		["protected"] MIMKey3 chgPattType;
		["protected"] string userTag;
		["protected"] string name;
		["protected"] MIMKey3Seq hypEventType;
		["protected"] FunctionSeq funcs;
    };

	["java:getset"]
    class RealFrqSpect extends DMDataEvent {
		["protected"] DoubleValue xAxisMin;
	    ["protected"] DoubleValue xAxisDelta;
	    ["protected"] DoubleSeq realValues;
    };

	["java:getset"]
    class RealWaveform extends DMDataEvent {
		["protected"] DoubleValue xAxisStart;
	    ["protected"] DoubleValue xAxisDelta;
	    ["protected"] DoubleSeq realValues;
    };	    
  
  	["java:getset"]
    class RUL extends ItemPrognosis {
		["protected"] DoubleValue rulValue;
		["protected"] DoubleValue error;
		["protected"] DoubleValue postConfid;
	};

    ["java:getset"]
	class RULDistrbn extends ItemPrognosis {
		["protected"] DoubleSeq ruls;
		["protected"] DoubleSeq cumulProbs;
		["protected"] DoubleSeq errors;
		["protected"] DoubleSeq postConfids;
	};

	["java:getset"]
	class SDDataEvent extends DataEvent {
		["protected"] DataStatus dataStatusValue;
		["protected"] NumAlertSeq numAlerts; 
	};

	["java:getset"]
	class SDEnum extends SDDataEvent {
		["protected"] EnumValue value;
	};

	["java:getset"]
	class SDEnumSetDataItem {
		["protected"] EnumValue value;
		["protected"] string tag;
	};

	["java:getset"]
	class SDEnumSet extends SDDataEvent {
		["protected"] SDEnumSetDataItemSeq values;
	};

	["java:getset"]
	class SDEvent extends SDDataEvent {
		["protected"] ItemEventSeq itemEvents;
	};

	["java:getset"]
	class SDInt extends SDDataEvent {
		["protected"] IntValue value;
	};

	["java:getset"]
	class SDPort extends Port {
		["protected"] EngUnit stateEU;
		["protected"] EngUnit measureEU;
		["protected"] MeasLoc measLocVal;
		["protected"] AlertRegionSeq  alertRegs;
		["protected"] ItemEventConfigSeq itemEventConfigVal;
	};

	["java:getset"]
	class SDReal extends SDDataEvent {
		["protected"] DoubleValue value;
	};

	["java:getset"]
	class SDTestInt extends SDDataEvent {
		["protected"] EnumValue evaluation;
		["protected"] IntValue measure;
    };

	["java:getset"]
	class SDTestReal extends SDDataEvent {
	    ["protected"] EnumValue evaluation;
		["protected"] DoubleValue measure;
    };

	["java:getset"]
	class Segment extends Item {
		["protected"] BooleanValue segmGroup;
		["protected"] MIMKey3 segmentType;
	};

	["java:getset"]
	class ShortArrayValue extends Value {
		["protected"] ShortSeq values;
	};

	["java:getset"]
	class StringArrayValue extends Value {
		["protected"] StringSeq values;
	};


	["java:getset"]
	class StringValue extends Value {
		["protected"] string value;
	};
	  
	["java:getset"]
	class UserDef extends DMDataEvent {
		["protected"] Data value;
	};

	["java:getset"]
	class ShortValue extends Value{
		["protected"] short value;
	};
  
 };
};

#endif