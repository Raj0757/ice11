// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class DMIntPrxHelper extends Ice.ObjectPrxHelperBase implements DMIntPrx
{
    public static DMIntPrx checkedCast(Ice.ObjectPrx __obj)
    {
        DMIntPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof DMIntPrx)
            {
                __d = (DMIntPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    DMIntPrxHelper __h = new DMIntPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static DMIntPrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        DMIntPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof DMIntPrx)
            {
                __d = (DMIntPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    DMIntPrxHelper __h = new DMIntPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static DMIntPrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        DMIntPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    DMIntPrxHelper __h = new DMIntPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static DMIntPrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        DMIntPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    DMIntPrxHelper __h = new DMIntPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static DMIntPrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        DMIntPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof DMIntPrx)
            {
                __d = (DMIntPrx)__obj;
            }
            else
            {
                DMIntPrxHelper __h = new DMIntPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static DMIntPrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        DMIntPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            DMIntPrxHelper __h = new DMIntPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::DMDataEvent",
        "::osacbm::schema::DMInt",
        "::osacbm::schema::DataEvent"
    };

    public static String ice_staticId()
    {
        return __ids[2];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _DMIntDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _DMIntDelD();
    }

    public static void __write(IceInternal.BasicStream __os, DMIntPrx v)
    {
        __os.writeProxy(v);
    }

    public static DMIntPrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            DMIntPrxHelper result = new DMIntPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
