// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class FutureHealthPrxHelper extends Ice.ObjectPrxHelperBase implements FutureHealthPrx
{
    public static FutureHealthPrx checkedCast(Ice.ObjectPrx __obj)
    {
        FutureHealthPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof FutureHealthPrx)
            {
                __d = (FutureHealthPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    FutureHealthPrxHelper __h = new FutureHealthPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static FutureHealthPrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        FutureHealthPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof FutureHealthPrx)
            {
                __d = (FutureHealthPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    FutureHealthPrxHelper __h = new FutureHealthPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static FutureHealthPrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        FutureHealthPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    FutureHealthPrxHelper __h = new FutureHealthPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static FutureHealthPrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        FutureHealthPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    FutureHealthPrxHelper __h = new FutureHealthPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static FutureHealthPrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        FutureHealthPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof FutureHealthPrx)
            {
                __d = (FutureHealthPrx)__obj;
            }
            else
            {
                FutureHealthPrxHelper __h = new FutureHealthPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static FutureHealthPrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        FutureHealthPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            FutureHealthPrxHelper __h = new FutureHealthPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::FutureHealth",
        "::osacbm::schema::ItemPrognosis"
    };

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _FutureHealthDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _FutureHealthDelD();
    }

    public static void __write(IceInternal.BasicStream __os, FutureHealthPrx v)
    {
        __os.writeProxy(v);
    }

    public static FutureHealthPrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            FutureHealthPrxHelper result = new FutureHealthPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
