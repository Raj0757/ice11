// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class EPSStatusPrxHelper extends Ice.ObjectPrxHelperBase implements EPSStatusPrx
{
    public static EPSStatusPrx checkedCast(Ice.ObjectPrx __obj)
    {
        EPSStatusPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof EPSStatusPrx)
            {
                __d = (EPSStatusPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    EPSStatusPrxHelper __h = new EPSStatusPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static EPSStatusPrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        EPSStatusPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof EPSStatusPrx)
            {
                __d = (EPSStatusPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    EPSStatusPrxHelper __h = new EPSStatusPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static EPSStatusPrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        EPSStatusPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    EPSStatusPrxHelper __h = new EPSStatusPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static EPSStatusPrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        EPSStatusPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    EPSStatusPrxHelper __h = new EPSStatusPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static EPSStatusPrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        EPSStatusPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof EPSStatusPrx)
            {
                __d = (EPSStatusPrx)__obj;
            }
            else
            {
                EPSStatusPrxHelper __h = new EPSStatusPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static EPSStatusPrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        EPSStatusPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            EPSStatusPrxHelper __h = new EPSStatusPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::EPSStatus"
    };

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _EPSStatusDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _EPSStatusDelD();
    }

    public static void __write(IceInternal.BasicStream __os, EPSStatusPrx v)
    {
        __os.writeProxy(v);
    }

    public static EPSStatusPrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            EPSStatusPrxHelper result = new EPSStatusPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
