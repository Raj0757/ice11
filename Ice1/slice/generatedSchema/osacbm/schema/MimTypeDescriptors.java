// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class MimTypeDescriptors extends Ice.ObjectImpl
{
    public MimTypeDescriptors()
    {
    }

    public MimTypeDescriptors(MIMKey3 postScalType, WindowType windowTypeValue, MIMKey3 srcDetectType, DoubleValue xAxisMax, DoubleValue xAxisMin)
    {
        this.postScalType = postScalType;
        this.windowTypeValue = windowTypeValue;
        this.srcDetectType = srcDetectType;
        this.xAxisMax = xAxisMax;
        this.xAxisMin = xAxisMin;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new MimTypeDescriptors();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::MimTypeDescriptors"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(postScalType);
        __os.writeObject(windowTypeValue);
        __os.writeObject(srcDetectType);
        __os.writeObject(xAxisMax);
        __os.writeObject(xAxisMin);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    postScalType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::WindowType";
                if(v == null || v instanceof WindowType)
                {
                    windowTypeValue = (WindowType)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    srcDetectType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 3:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    xAxisMax = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 4:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    xAxisMin = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.readObject(new Patcher(3));
        __is.readObject(new Patcher(4));
        __is.endReadSlice();
    }

    protected MIMKey3 postScalType;

    public MIMKey3
    getPostScalType()
    {
        return postScalType;
    }

    public void
    setPostScalType(MIMKey3 _postScalType)
    {
        postScalType = _postScalType;
    }

    protected WindowType windowTypeValue;

    public WindowType
    getWindowTypeValue()
    {
        return windowTypeValue;
    }

    public void
    setWindowTypeValue(WindowType _windowTypeValue)
    {
        windowTypeValue = _windowTypeValue;
    }

    protected MIMKey3 srcDetectType;

    public MIMKey3
    getSrcDetectType()
    {
        return srcDetectType;
    }

    public void
    setSrcDetectType(MIMKey3 _srcDetectType)
    {
        srcDetectType = _srcDetectType;
    }

    protected DoubleValue xAxisMax;

    public DoubleValue
    getXAxisMax()
    {
        return xAxisMax;
    }

    public void
    setXAxisMax(DoubleValue _xAxisMax)
    {
        xAxisMax = _xAxisMax;
    }

    protected DoubleValue xAxisMin;

    public DoubleValue
    getXAxisMin()
    {
        return xAxisMin;
    }

    public void
    setXAxisMin(DoubleValue _xAxisMin)
    {
        xAxisMin = _xAxisMin;
    }

    public static final long serialVersionUID = -94647130L;
}
